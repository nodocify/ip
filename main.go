package main

import (
	"gitlab.com/nodocify/ip/cmd"
)

func main() {
	cmd.Execute()
}

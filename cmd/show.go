package cmd

import (
	"fmt"
	"log"
	"net"
	"strings"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(showCmd)
}

var showCmd = &cobra.Command{
	Use:   "show",
	Short: "Show ip information for all interfaces.",
	Args:  cobra.MaximumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		ifaces, err := net.Interfaces()
		if err != nil {
			log.Fatal("No interfaces found")
		}
		for _, i := range ifaces {
			addrs, err := i.Addrs()
			if err != nil {
				fmt.Print("No addresses found")
				continue
			}
			switch len(args) {
			case 0:

				for _, a := range addrs {
					fmt.Printf("%v\n    %v\n", i.Name, a)
				}
			case 1:
				if args[0] == i.Name || strings.HasPrefix(strings.ToLower(i.Name), strings.ToLower(args[0])) {
					for _, a := range addrs {
						fmt.Printf("%v\n    %v\n", i.Name, a)
					}
				}
			}
		}
	},
}

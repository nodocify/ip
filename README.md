ip


## End goals of this project

1. Be able to show all interfaces
    
    `ip show`

2. Be able to inquire the current settings of an interface

    `ip show <interface_name>`

2. Be able to set an interface to DHCP.

    `ip set <interface_name> dhcp`

3. Be able to set an interface to static

    `ip set <interface_name> static <address> <mask> [<gateway>]`

4. Be able to set an interface DNS servers.

    `ip set <interface_name> dns 8.8.8.8 1.1.1.1`